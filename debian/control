Source: cylc
Section: utils
Priority: optional
Maintainer: Alastair McKinstry <mckinstry@debian.org>
Build-Conflicts: cylc
Build-Depends: debhelper (>= 10), 
 dh-python,  
 python-all-dev, 
 python-pygraphviz, 
 pyro, 
 python-jinja2, 
 python-gtk2,
 texlive-latex-extra, 
 texlive-fonts-recommended,  
 lmodern, 
 tex4ht, 
 imagemagick, sqlite3,
 tex-gyre, 
 python-markupsafe, 
 python-cherrypy3
Standards-Version: 4.2.0
Homepage: https://cylc.github.io/cylc
Vcs-Browser: https://salsa.debian.org:/science-team/cylc.git
Vcs-Git: https://salsa.debian.org:/science-team/cylc.git

Package: cylc
Depends: ${misc:Depends}, ${shlibs:Depends}, ${python:Depends},
 sqlite3, python-cylc, python, at, perl
Architecture: all
Description: Workflow scheduler 
 Cylc ("silk") is a suite engine and meta-scheduler that specializes
 in suites of cycling tasks for weather forecasting, climate modeling,
 and related processing (it can also be used for one-off workflows
 of non-cycling tasks, which is a simpler problem).

Package: python-cylc
Section: python
Architecture:  any
Breaks: xdot (<< 0.7-2)
Replaces: xdot (<< 0.7-2)
Depends: ${python:Depends}, ${misc:Depends}, pyro, 
 python-sqlite, python-gtk2, python-jinja2, python-pygraphviz,
 python-markupsafe, python-cherrypy3, pyro
Description: Python libraries for cylc workflow scheduler
 Cylc ("silk") is a suite engine and meta-scheduler that specializes
 in suites of cycling tasks for weather forecasting, climate modeling,
 and related processing (it can also be used for one-off workflows
 of non-cycling tasks, which is a simpler problem).
 .
 This package contains python library code used by cylc.

